/* Base device object */

#include "serial_depth.h"

SerialDepth::SerialDepth()
{
}

SerialDepth::~SerialDepth()
{

  if (mPort)
    delete mPort;
}

int SerialDepth::init(string portName, int baud, int timeout)
{
  //Attempt to open serial device:
  try
    {
      mPort = new serial::Serial(portName, baud, serial::Timeout::simpleTimeout(timeout));
    }
  catch (serial::IOException e)
    {
      cout << __FILE__ << ": Unable to open port:" << portName << endl;
      return DEPTH_FAILURE;
    }
  
  if (!mPort->isOpen())
    {
      //Unable to open serial port for some reason
      cout <<  __FILE__ << ": Unable to open port:" << portName << endl;
      return DEPTH_FAILURE;
    }

  vector<uint16_t> depthValues;
  int retVal;
  retVal = getValues(depthValues); // pull off the first (potentially incomplete bytes)
  if (retVal == DEPTH_FAILURE)
    {
      cout << __FILE__ << ": Junk received on init, flushing" << endl;
    }
  return DEPTH_SUCCESS;
}

int SerialDepth::getValues(vector<uint16_t> &results)
{
  /* Pull off a new reading, newline terminated */

  string rawResult = mPort->readline();

  if (rawResult.length() < 6)
    {
      //No results received
      //cout << "No result!" << endl;
      return DEPTH_FAILURE;
    }

  //Parse the result to strip the header cruft and return a uint16_t
  results.clear();
  
  //cout << __FILE__ << " length:" << rawResult.length() << " result:" << rawResult << endl;

  //Split the string into individual tokens
  vector<string> splits = split<string>(rawResult, "\t");

  //This is size-1 because the data stream is terminated as \t\r\n, which gets registered as another token
  for (int i =0; i < splits.size() - 1; i++)
    {
 
      unsigned long depth = strtoul(splits[i].c_str(), NULL, 10);
      lastValue = (uint16_t) depth;
      results.push_back(lastValue);
    }

  return DEPTH_SUCCESS;
}


