/* ROS node for the serial IR sensor */

#include "serial_depth.h"
#include "std_msgs/UInt16MultiArray.h"
#include "ros/ros.h"

namespace serial_depth
{
  class SDNode
  {
  public:
    SDNode(ros::NodeHandle& nh);
    ~SDNode();
    void init();
    int getDepths(vector<uint16_t> &out);
    SerialDepth *ctl;
    string portName;
    int baud;
    int timeout;
    ros::NodeHandle nh_;
  };
  
  SDNode::SDNode(ros::NodeHandle& nh)
  {
    nh_ = nh;
    init();
  }

  SDNode::~SDNode() {}

  void SDNode::init()
  {
    ctl = new SerialDepth();

    portName = "/dev/ttyACM0";
    baud = 115200;
    timeout = 1000;

    
    //Pick up the port, baud, etc from the  file via the NodeHandle
    nh_.getParam("port", portName);
    nh_.getParam(string("baud"), baud);
    nh_.getParam(string("timeout"), timeout);

    
    
    ROS_INFO("SDNode: Using %s at %d baud", portName.c_str(), baud);

    int ret;

    ret = ctl->init(portName, baud, timeout);
    if (ret == ctl->DEPTH_FAILURE)
      {
	ROS_FATAL("Unable to open port name: [%s]", portName.c_str());
	return;
      }

  }

  int SDNode::getDepths(vector<uint16_t> &out)
  {
    return ctl->getValues(out);
  }

}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "serial_depth_node");
  ros::NodeHandle nh("~");
  
 
  serial_depth::SDNode node(nh);

  
  ros::Publisher depth_pub = nh.advertise<std_msgs::UInt16MultiArray>("depths", 1000);

  vector<uint16_t> depths;
  int retVal;
  
  while (ros::ok())
    {

      std_msgs::UInt16MultiArray msg;
      std_msgs::MultiArrayDimension dim;
      dim.label = "depth";
      dim.size = 0;
      msg.layout.dim.push_back(dim);
      msg.layout.data_offset = 0;
      
      
      //Runs at hardware rate as the messages are published
      retVal = node.getDepths(depths);

      //Parse the vector and populate the msg
      for (int i=0; i<depths.size(); i++)
	{
	  msg.data.push_back(depths[i]);
	}
      msg.layout.dim[0].size = depths.size();

      
      depth_pub.publish(msg);
      
      ros::spinOnce();
    }


  return 0;
}
