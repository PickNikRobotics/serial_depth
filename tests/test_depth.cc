/* Test the finger depth sensor serial device object */
#include <string>
#include <iostream>
#include <stdlib.h>

#include "serial_depth.h"

using std::cout;
using std::endl;

int main(int argc, char** argv)
{
  SerialDepth ctl;
  string portName;
  int baud = 115200;
  
  if (argc > 2)
    portName = string(argv[1]);
  else
    portName = string("/dev/serial_depth");
  
  if (argc > 3)
    baud = atoi(argv[2]);

  printf("Opening port: %s\n", portName.c_str());
  
  int ret = ctl.init(portName, baud, 1000);
  if (ret == ctl.DEPTH_FAILURE)
    {
      printf("Bad port name: %s\n", portName.c_str());
      exit(1);
    }
  
  vector<uint16_t> depths;
  int retVal;
  while (1)
    {
      retVal = ctl.getValues(depths);
      if (retVal == ctl.DEPTH_SUCCESS)
	cout << "Depths size:" <<  depths.size() << endl;
      
    }
}
