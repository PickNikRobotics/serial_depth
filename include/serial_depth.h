#ifndef _SERIAL_DEPTH_H_
#define _SERIAL_DEPTH_H_
/* C++ interface to a Arduino spitting out depth data */


#include <string>
#include <iostream>
#include <unistd.h>
#include <serial/serial.h>
#include <stdlib.h>

using std::string;
using std::cout;
using std::endl;
using std::vector;

template<typename T>
vector<T> split(const T & str, const T & delimiters)
{
  vector<T> v;
    typename T::size_type start = 0;
    size_t pos = str.find_first_of(delimiters, start);
    while(pos != T::npos) {
        if(pos != start) // ignore empty tokens
	  v.push_back(str.substr(start, pos - start));
        start = pos + 1;
        pos = str.find_first_of(delimiters, start);
    }
    if(start < str.length()) // ignore trailing delimiter
      v.push_back(str.substr(start, pos - start)); // add what's left of the string
    return v;
}


class SerialDepth
{
 public:
  SerialDepth();
  ~SerialDepth();
  int init(string portName, int baud, int timeout);


  int getDepth(string &result);
  static const int DEPTH_FAILURE = -1;
  static const int DEPTH_SUCCESS = 1;
  int getValues(vector<uint16_t> &results);
 private:
  int sendCommand(string m_cmd);

  serial::Serial *mPort;
  uint16_t lastValue;

};


#endif
