# Serial Depth driver

## Install


Listens to a given serial port, expecting a tab-separated list of uint16_t values:

123\t123\t1231\n
=======
You may need

    sudo chown $USER:$USER ttyACM0

if not using the included UDev rule

## Run


    roslaunch serial_depth serial_depth.launch

    rostopic echo depths
	
Launch file has the following params to set:

```
port: Serial port to connect to
baud: Baud rate to use
timeout: ms to wait for data on the port via select()
```

## Details

Listens to a given serial port, expecting a semicolon-separated list of uint16_t values:

    123;123;1231\n

Publishes as a ROS topic 'depths' as a UInt16MultiArray


Built on the serial library 
=======
serial_depth ROS node for the i2c depth sensor

Tested against the three-sensor version

Includes a UDev rule for the FTDI cable under config/55-serial-depth.rules
